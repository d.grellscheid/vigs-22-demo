import sys

def collatz_step(n):
    """Get the next number in the collatz sequence"""
    return n//2 if n%2 == 0 else 2*n + 1


def collatz_len(N):
    return 1, 1

if __name__ == "__main__":
    # should really use argparse here
    if len(sys.argv) < 2:
        print(f"Usage: {__file__} NNN")
        exit(1)

    max_N = int(sys.argv[1])
    start, length = collatz_len(max_N)
    print(f"The longest collatz chain up to {max_N} starts at {start} and has {length} elements.")
